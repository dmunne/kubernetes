# Kubernetes playground
## Summary
Create a simple k8s environment (single-master with N workers) with Vagrant.
## System Requirements
- [Vagrant](https://www.vagrantup.com/)
- [Libvirt](https://docs.fedoraproject.org/en-US/quick-docs/getting-started-with-virtualization/)
- [Ansible](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html)
## VMs Requirements
This values can be modified in the Vagrantfile depending on the laptop resources:

| **Master** | 3 vCPU | 4Gb RAM |
| :------: | :-------: | :-------: |
| **Workers** | 1 vCPU | 2Gb RAM |

## Create Playground
1. Clone the repository
```
$ git clone https://gitlab.com/dmunne/kubernetes.git
```
2. Create VMs
```
$ vagrant up
```
3. Install and configure Kubernetes
```
$ cd kubernetes-setup
$ ansible-playbook install_k8s.yaml [ --extra-vars k8s_version=1.17.1 ]
```
4. Play! 
```
$ vagrant ssh master
vagrant@master:~$ kubectl get nodes
NAME       STATUS   ROLES    AGE    VERSION
master     Ready    master   101s   v1.19.1
worker-1   Ready    <none>   57s    v1.19.1
worker-2   Ready    <none>   57s    v1.19.1
```
